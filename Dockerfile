FROM registry.hzdr.de/fwf/libraries/dockerfiles/docker-epics-ioc-base:1.1.0

# calc
ARG EPICS_CALC_VERSION=R3-7-5
ENV EPICS_CALC=${EPICS_SUPPORT}/calc
WORKDIR ${EPICS_CALC}
RUN mkdir build \
    && cd build \
    && git clone --depth 1 --branch ${EPICS_CALC_VERSION} --recursive https://github.com/epics-modules/calc.git . \
    && echo "SUPPORT=${EPICS_SUPPORT}" >> configure/RELEASE.local \
    && echo "SSCAN=" >> configure/RELEASE.local \
    && echo "EPICS_BASE=${EPICS_BASE}" >> configure/RELEASE.local \
    && make -j$(nproc) \
    && cp -r lib include db dbd configure ${EPICS_CALC}/ \
    && cd .. \
    && rm -r build

# asyn
ARG EPICS_ASYN_VERSION=R4-44-2
ENV EPICS_ASYN=${EPICS_SUPPORT}/asyn
WORKDIR ${EPICS_ASYN}
RUN mkdir build \
    && cd build \
    && git clone --depth 1 --branch ${EPICS_ASYN_VERSION} --recursive https://github.com/epics-modules/asyn.git . \
    && echo "SUPPORT=${EPICS_SUPPORT}" >> configure/RELEASE.local \
    && echo "CALC=${EPICS_CALC}" >> configure/RELEASE.local \
    && echo "EPICS_BASE=${EPICS_BASE}" >> configure/RELEASE.local \
    && echo "TIRPC=YES" >> configure/RELEASE.local \
    && make -j$(nproc) \
    && cp -r bin lib include db dbd configure ${EPICS_ASYN}/ \
    && cd .. \
    && rm -r build

# lb115 support module
ENV EPICS_LB115=${EPICS_SUPPORT}/lb115
COPY . ${EPICS_LB115}/build/
WORKDIR ${EPICS_LB115}
RUN cd build \
    && echo "SUPPORT=$EPICS_SUPPORT" >> configure/RELEASE.local \
    && echo "CALC=$EPICS_CALC" >> configure/RELEASE.local \
    && echo "ASYN=$EPICS_ASYN" >> configure/RELEASE.local \
    && echo "ALIVE=" >> configure/RELEASE.local \
    && echo "SNCSEQ=" >> configure/RELEASE.local \
    && echo "EPICS_BASE=$EPICS_BASE" >> configure/RELEASE.local \
    && make -j$(nproc) \
    && cp -r bin lib db dbd configure ${EPICS_LB115}/ \
    && cd .. \
    && rm -r build

# the ioc
ENV IOC_TOP=${EPICS_INSTALL_PREFIX}/lb115-ioc
ENV IOC_APPNAME=lb115
ENV IOC_BOOT=${IOC_TOP}/iocBoot/ioc${IOC_APPNAME}
WORKDIR ${IOC_TOP}
RUN makeBaseApp.pl -t ioc -u epics ${IOC_APPNAME}
RUN makeBaseApp.pl -i -t ioc -u epics -p ${IOC_APPNAME} -a ${EPICS_HOST_ARCH} ${IOC_APPNAME}
COPY docker/Makefile ${IOC_APPNAME}App/src/
COPY docker/st.cmd ${IOC_BOOT}/
COPY lb115Sup/Db/ ${IOC_TOP}/${IOC_APPNAME}App/Db/
RUN echo "SUPPORT=$EPICS_SUPPORT" >> configure/RELEASE.local \
    && echo "AUTOSAVE=$EPICS_AUTOSAVE" >> configure/RELEASE.local \
    && echo "CALC=$EPICS_CALC" >> configure/RELEASE.local \
    && echo "ASYN=$EPICS_ASYN" >> configure/RELEASE.local \
    && echo "LB115=$EPICS_LB115" >> configure/RELEASE.local \
    && echo "IOCSTATS=$EPICS_IOCSTATS" >> configure/RELEASE.local \
    && echo "RESYNC=$EPICS_RESYNC" >> configure/RELEASE.local \
    && echo "SNCSEQ=" >> configure/RELEASE.local \
    && echo "EPICS_BASE=$EPICS_BASE" >> configure/RELEASE.local \
    && make -j$(nproc)

# labels
LABEL maintainer="m.meyer@hzdr.de"
ARG BUILD_DATE
ARG BUILD_VERSION
ARG VCS_REF
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="lb115-ioc"
LABEL org.label-schema.description="EPICS IOC for the LB115 radiation monitor"
LABEL org.label-schema.url="https://hzdr.de/fwf/"
LABEL org.label-schema.vcs-url="https://codebase.helmholtz.cloud/fwf/Libraries/epics/lb115-ioc.git"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vendor="FWF"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.docker.cmd="docker run -dit -v ./lb115-ioc-boot:/usr/local/epics/lb115-ioc/iocBoot/ioclb115 -v ./lb115-ioc-db:/usr/local/epics/lb115-ioc/db --network host -e PUID=1000 -e PGID=1000 --name LB115-IOC lb115-ioc"

# start the ioc
WORKDIR ${IOC_BOOT}
COPY docker/init.sh /init.sh
RUN chmod +x ./st.cmd \
    && chmod +x /init.sh \
    && mkdir /default_config \
    && cp -r ${IOC_BOOT}/. /default_config/iocBoot \
    && cp -r ${IOC_TOP}/db/. /default_config/iocDb
EXPOSE 5064/udp
EXPOSE 5065/udp
HEALTHCHECK --interval=60s --timeout=5s --start-period=180s --retries=3 CMD caget $(head -n 1 records.txt)

CMD /init.sh

