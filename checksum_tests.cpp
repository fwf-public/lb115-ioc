/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <string.h>
#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm>

std::string calculateChecksum(const std::string &input)
{
    
  std::string in = input;
  //int ll = 254;
  //in = in.substr(in.length()-ll,ll);
    
  uint32_t sum = 0;
  for (std::string::size_type i = 0; i < in.size(); ++i)
  {
      sum += uint8_t(in[i]);
      
      printf("%ld (%c, 0x%x): 0x%x\n", i, uint8_t(in[i]), uint8_t(in[i]), sum);
  }

  std::stringstream stream;
  stream << std::setfill('0') << std::setw(2) << std::uppercase
          << std::hex << sum;
  return stream.str().substr(stream.str().length()-2,2);
}

std::string extractChecksum(const std::string &input, std::size_t &startOfCsm)
{
  startOfCsm = std::string::npos; 
  std::size_t beforeCsm = input.find("*", 1);
  if(beforeCsm != std::string::npos)
  {
    std::size_t afterCsm = input.find("*", beforeCsm + 1);
    if(afterCsm != std::string::npos)
    {
      startOfCsm = beforeCsm + 1;
      return input.substr(beforeCsm + 1, afterCsm - beforeCsm - 1);
    }
  }
  return "";
}

bool checkChecksum(const std::string &input, std::string &inputWithoutCsm, std::string &parsed, std::string &calculated)
{
  std::size_t startOfCsm;
  inputWithoutCsm = input;
  parsed = extractChecksum(input, startOfCsm);
  if(parsed.length() > 0 && startOfCsm != std::string::npos)
  {
    inputWithoutCsm = input.substr(0, startOfCsm);
    printf("%s\n", inputWithoutCsm.c_str());
    calculated = calculateChecksum(inputWithoutCsm);
    return parsed == calculated;
  }
  // if no checksum was found, consider as OK
  return true;
}

int main()
{
#if 0 // OK
    std::string responseString = "*0001900101020109*6C*";
#endif
    
#if 1 // NOT OK
    std::string responseString = "*9001000101020109read_index:act|memory_index:1131|detector_name:LB6360-H10|detector_tag:LB6360-H10|meas_id:Det1Beta|meas_id_name:Det1 ";
    responseString += 0xdf;
    responseString += "|meas_date:26.06.2023 16:25:17|meas_status:0x048000|status:0x000EFF|meas_val:Tmout|max_meas_val:Tmout|unit_meas_val:";
    responseString += 0xb5;
    responseString += "Sv/h|uncertainty_abs:Tmout|uncertainty_rel:Tmout|detection_limit:Tmout|decision_thres:Tmout|best_est:Tmout|unc_best_est:Tmout|lower_limit_conf:Tmout|";
    responseString += "upper_limit_conf:Tmout|alarm_limit_1:1|alarm_limit_2:2|alarm_limit_3:3|alarm_limit_4:4|calib_factor:0.123|dose_val:0.000|unit_dose_val:";
    responseString += 0xb5;
    responseString += "Sv|dose_time:19423|gross_val:0.000|net_val:0.000|bkg_val:0.000|meas_time:281|bkg_meas_time:3600|delta_scint_factor:2.01|*5F*";
#endif

#if 0 // NOT OK
    std::string responseString = "*9001000101030109read_index:act|memory_index:1131|detector_name:LB6500-4D|detector_tag:LB6500-4D|meas_id:Det2Alpha|meas_id_name:Det2 ";
    responseString += "a|meas_date:26.06.2023 16:25:17|meas_status:0x040000|status:0x000EFF|meas_val:Tmout|max_meas_val:Tmout|unit_meas_val:";
    responseString += 0xb5;
    responseString += "Sv/h|uncertainty_abs:Tmout|uncertainty_rel:Tmout|detection_limit:Tmout|decision_thres:Tmout|best_est:Tmout|unc_best_est:Tmout|lower_limit_conf:Tmout|";
    responseString += "upper_limit_conf:Tmout|alarm_limit_1:1|alarm_limit_2:2|alarm_limit_3:3|alarm_limit_4:4|calib_factor:1|dose_val:0.000|unit_dose_val:";
    responseString += 0xb5;
    responseString += "Sv|dose_time:19419|gross_val:0.000|net_val:0.000|bkg_val:0.000|meas_time:41|bkg_meas_time:3600|delta_scint_factor:2.01|*65*";
#endif

#if 0 // NOT OK
    std::string responseString = "*9001000101010109read_index:act|memory_index:1208|detector_name:LB6360-H10|detector_tag:LB6360-H10|meas_id:Det1Alpha|meas_id_name:Det1 ";
    responseString += "a|meas_date:27.06.2023 08:50:21|meas_status:0x048000|status:0x000EFF|meas_val:Tmout|max_meas_val:Tmout|unit_meas_val:";
    responseString += 0xb5;
    responseString += "Sv/h|uncertainty_abs:Tmout|uncertainty_rel:Tmout|detection_limit:Tmout|decision_thres:Tmout|best_est:Tmout|unc_best_est:Tmout|lower_limit_conf:Tmout|";
    responseString += "upper_limit_conf:Tmout|alarm_limit_1:1|alarm_limit_2:2|alarm_limit_3:3|alarm_limit_4:4|calib_factor:1|dose_val:0.000|unit_dose_val:";
    responseString += 0xb5;
    responseString += "Sv|dose_time:23477|gross_val:0.000|net_val:0.000|bkg_val:0.000|meas_time:19|bkg_meas_time:3600|delta_scint_factor:2.01|*D7*";
    // ee87
#endif


#if 0 // NOT OK
    std::string responseString = "*9001000101010109read_index:act|memory_index:1208|detector_name:LB6360-H10|detector_tag:LB6360-H10|meas_id:Det1Alpha|meas_id_name:Det1 ";
    responseString += "a|meas_date:27.06.2023 08:50:22|meas_status:0x048000|status:0x000EFF|meas_val:Tmout|max_meas_val:Tmout|unit_meas_val:";
    responseString += 0xb5;
    responseString += "Sv/h|uncertainty_abs:Tmout|uncertainty_rel:Tmout|detection_limit:Tmout|decision_thres:Tmout|best_est:Tmout|unc_best_est:Tmout|lower_limit_conf:Tmout|";
    responseString += "upper_limit_conf:Tmout|alarm_limit_1:1|alarm_limit_2:2|alarm_limit_3:3|alarm_limit_4:4|calib_factor:1|dose_val:0.000|unit_dose_val:";
    responseString += 0xb5;
    responseString += "Sv|dose_time:23478|gross_val:0.000|net_val:0.000|bkg_val:0.000|meas_time:20|bkg_meas_time:3600|delta_scint_factor:2.01|*D1*";
    // ee81
#endif

#if 0 // NOT OK
    std::string responseString = "*9001000101010109read_index:act|memory_index:1208|detector_name:LB6360-H10|detector_tag:LB6360-H10|meas_id:Det1Alpha|meas_id_name:Det1 ";
    responseString += "a|meas_date:27.06.2023 08:50:23|meas_status:0x048000|status:0x000EFF|meas_val:Tmout|max_meas_val:Tmout|unit_meas_val:";
    responseString += 0xb5;
    responseString += "Sv/h|uncertainty_abs:Tmout|uncertainty_rel:Tmout|detection_limit:Tmout|decision_thres:Tmout|best_est:Tmout|unc_best_est:Tmout|lower_limit_conf:Tmout|";
    responseString += "upper_limit_conf:Tmout|alarm_limit_1:1|alarm_limit_2:2|alarm_limit_3:3|alarm_limit_4:4|calib_factor:1|dose_val:0.000|unit_dose_val:";
    responseString += 0xb5;
    responseString += "Sv|dose_time:23479|gross_val:0.000|net_val:0.000|bkg_val:0.000|meas_time:21|bkg_meas_time:3600|delta_scint_factor:2.01|*D4*";
    // ee84
#endif
    
#if 0 // OK
    std::string responseString = "*900100010104010006.02.2020 10:55:24:38:LB1342:LB1342:000000000EFF:0.990:0.990:cps:0.640:64.66:1.131:0.000:1.072:0.567:0.109:2.260:1:82.56:cts:76698:0.990:0.990:0.000:10:3600:CT*A5*";
#endif
    
    // checksum
    std::string inputWithoutCsm;
    std::string parsed;
    std::string calculated;
    if(!checkChecksum(responseString, inputWithoutCsm, parsed, calculated))
    {
        printf("WARN: Checksum does not match: %s parsed, %s expected\r\n", parsed.c_str(), calculated.c_str());
    }
    
    printf("parsed: %s\ncalculated: %s\n", parsed.c_str(), calculated.c_str());

    return 0;
}
