#!/bin/bash

# add runtime user
USER_ID=${PUID:-9001}
GROUP_ID=${PGID:-9001}
useradd -u $USER_ID -o -m user
groupmod -g $GROUP_ID user
export HOME=/home/user

# copy default configuration if exported binds are empty
if [ -z "$(ls -A $IOC_BOOT)" ]; then
   cp -r /default_config/iocBoot/. $IOC_BOOT
fi
if [ -z "$(ls -A $IOC_TOP/db)" ]; then
   cp -r /default_config/iocDb/. $IOC_TOP/db
fi

# grant runtime user access to exported binds
chown -R $USER_ID:$GROUP_ID /$IOC_BOOT
chmod 774 -R /$IOC_BOOT
chown -R $USER_ID:$GROUP_ID /$IOC_TOP/db
chmod 774 -R /$IOC_TOP/db

# run the executeable as runtime user
runuser user -c "./st.cmd"
