#!../../bin/linux-x86_64/lb115App

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/lb115App.dbd"
lb115App_registerRecordDeviceDriver pdbbase

## Asyn Config
drvAsynIPPortConfigure("L0","192.168.43.18:8123",0,0,0)

## Run this to trace the stages of iocInit
#asynSetTraceMask("L0", 0xFF, 0xFF)
#asynSetTraceIOMask("L0", 0xFF, 0xFF)

## Load the lb115 driver
Lb115CreateController("TF01", "L0", "200", "0001", "9001"))

## Load record instances
dbLoadRecords("db/lb115.db","P=IC:TF01,PORT=TF01")

## digial I/O
dbLoadRecords("db/lb115di.db","P=IC:TF01,PORT=TF01,ID=1")
dbLoadRecords("db/lb115di.db","P=IC:TF01,PORT=TF01,ID=2")
dbLoadRecords("db/lb115di.db","P=IC:TF01,PORT=TF01,ID=3")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=1")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=2")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=3")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=4")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=5")

## detector info
dbLoadRecords("db/lb115detector.db","P=IC:TF01,PORT=TF01,DET=1")
dbLoadRecords("db/lb115detector.db","P=IC:TF01,PORT=TF01,DET=2")

## channels
### Detector 1 Alpha
dbLoadRecords("db/lb115channel.db","P=IC:TF01,PORT=TF01,CHANNEL=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=1,LIMIT=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=1,LIMIT=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=1,LIMIT=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=1,LIMIT=4")

### Detector 1 Beta
dbLoadRecords("db/lb115channel.db","P=IC:TF01,PORT=TF01,CHANNEL=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=2,LIMIT=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=2,LIMIT=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=2,LIMIT=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=2,LIMIT=4")

### Detector 2 Alpha
dbLoadRecords("db/lb115channel.db","P=IC:TF01,PORT=TF01,CHANNEL=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=3,LIMIT=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=3,LIMIT=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=3,LIMIT=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=3,LIMIT=4")

### Detector 2 Beta
dbLoadRecords("db/lb115channel.db","P=IC:TF01,PORT=TF01,CHANNEL=4")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=4,LIMIT=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=4,LIMIT=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=4,LIMIT=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=4,LIMIT=4")

cd "${TOP}/iocBoot/${IOC}"
iocInit

dbl > records.txt
