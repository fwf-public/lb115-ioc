# 1.1.0 released 15.05.2023
- explicitly set epics-ioc-base docker base image to 1.1.0

# 1.0.0 released 13.12.2023
- Initial release
