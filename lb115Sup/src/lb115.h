/********************************************************************
* Copyright (c) 2023 Helmholtz-Zentrum Dresden-Rossendorf e.V.
* 
* lb115-ioc is a distributed subject to a Software License Agreement
* found in file LICENSE that is included with this distribution.
********************************************************************/

#include <stddef.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <queue>
#include <map>

#include <epicsTypes.h>
#include <epicsExit.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExport.h>
#include <iocsh.h>

#include <asynOctetSyncIO.h>
#include <asynPortDriver.h>

class epicsShareClass Lb115Controller : public asynPortDriver 
{
public:
  Lb115Controller(const char *portName, const char *Lb115PortName, double pollPeriod, 
                  const char* lb115Id = "0001", const char* localId = "9001");
  ~Lb115Controller();

  void init();
  void requestThread();
  void parseThread();
  void stopThreads(void);
  
  asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value) override;
  void report(FILE *fp, int details) override;
    
protected:
  asynUser *m_pasynUserController;
  epicsEventId m_requestEventId;
  epicsEventId m_parseEventId;

  struct sCommand
  {
    std::string message;
    int paramIndex = -1;
    bool expectsResponse = false;
    int channel = -1;
  };
  struct sResponse
  {
    std::string message;
    std::string request;
    int paramIndex = -1;
    int channel = -1;
  };

  std::queue<sCommand> m_commandQueue;
  std::queue<sResponse> m_responseQueue;

  // string split into vector
  void tokenize(std::string const &str, const char delim, std::vector<std::string> &out);

  // checksum methods
  std::string calculateChecksum(const std::string &input);
  std::string extractChecksum(const std::string &input, std::size_t &startOfCsm);
  bool checkChecksum(const std::string &input, std::string &inputWithoutCsm, std::string &parsed, std::string &calculated);

  static const int m_relaisCount = 5;
  static const int m_digitalInCount = 3;
  static const int m_alarmPerChannelCount = 4;
  static const int m_channelCount = 4;
  static const int m_detectorCount = 2;

  enum eChannelStates
  {
    eInvalidState = 0,
    eOk = 1,
    eUnderrange = 2,
    eOverrange = 3,
    eTimeout = 4,
    eError = 5,
  };

  enum eDataPoints
  {
    eInvalidDataPoint = 0,
    eDateTime,
    eProgramVersion,
    eKernelVersion,
    eSerialNumber,
    eDeviceName,
    eDeviceState,

    eStartMeas,
    eStopMeas,
    eResetDoseA,
    eResetDoseB,

    eDet1HwVersion,
    eDet1SwVersion,
    eDet1Alpha,
    eDet1Beta,

    eDet2HwVersion,
    eDet2SwVersion,
    eDet2Alpha,
    eDet2Beta,
  };

  const std::map<eDataPoints, std::string> m_mapDataPoints = {

    { eDateTime,        "00010103" },
    { eProgramVersion,  "03010100" },
    { eKernelVersion,   "03010101" },
    { eSerialNumber,    "03010104" },
    { eDeviceName,      "03010110" },
    { eDeviceState,     "03010109" },

    { eStartMeas,       "01000300" },
    { eStopMeas,        "01000301" },
    { eResetDoseA,      "01000302" },
    { eResetDoseB,      "01000303" },

    { eDet1HwVersion,   "03010600" },
    { eDet1SwVersion,   "03010200" },
    { eDet1Alpha,       "01010109" },
    { eDet1Beta,        "01020109" },

    { eDet2HwVersion,   "03020600" },
    { eDet2SwVersion,   "03020200" },
    { eDet2Alpha,       "01030109" },
    { eDet2Beta,        "01040109" },
  };

  // query setup methods
  void getProgramVersion();
  void getKernelVersion();
  void getSerialNumber();
  void getDeviceName();
  void getDeviceState();
  void getDetVersion(epicsInt32 detector);
  void getChannel(epicsInt32 channel);

  void setEnableMeasurement(epicsInt32 enable);
  void setReset(epicsInt32 detector);


  // properties
  bool m_exitPending = false;
  double m_sleepPeriod = 0.01;
  double m_pollPeriod = 0.1;
  std::string m_toDevice = "0001";
  std::string m_fromDevice = "9001";
  char m_inString[11111];
  int m_repeatCounter = 0;
  int m_comFailCounter = 0;

  // Indexes
  // status
  int m_indexProgramVersion;
  int m_indexKernelVersion;
  int m_indexSerialNumber;
  int m_indexDeviceName;
  int m_indexDigialIn[m_digitalInCount];
  int m_indexRelais[m_relaisCount];
  int m_indexMeasEnabled;
  int m_indexSetMeasEnable;
  int m_indexDoseMode; // Low or high dose mode
  int m_indexFail; // I2CTimeout, OutOfMemory

  struct sDetectorIndex
  {
    int swVersion;
    int hwVersion;
    int reset;
  };
  sDetectorIndex m_indexDetector[m_detectorCount];

  struct sChannelIndex
  {
    int detName;
    int id;
    int rateUnit;
    int rateValue;
    int state; // fail, lowerrange, overrange, timeout
    int rateTime;
    int rateAlarmValue[m_alarmPerChannelCount];
    int rateAlarmActive[m_alarmPerChannelCount];
    int timestamp;
    int datetime;
    int doseValue;
    int doseUnit;
    int doseTime;
  };
  sChannelIndex m_indexChannel[m_channelCount];

  // internal
  int m_indexPollPeriod;
  int m_indexConnected;
  int m_indexComFail;
};