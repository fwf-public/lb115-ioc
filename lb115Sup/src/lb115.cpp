/********************************************************************
* Copyright (c) 2023 Helmholtz-Zentrum Dresden-Rossendorf e.V.
* 
* lb115-ioc is a distributed subject to a Software License Agreement
* found in file LICENSE that is included with this distribution.
********************************************************************/

#include "lb115.h"
#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm>

static const char *driverName = "Lb115Controller";
extern "C"
{
  static void requestThreadC(void *drvPvt)
  {
      Lb115Controller *pPvt = (Lb115Controller *)drvPvt;
      pPvt->requestThread();
  }

  static void parseThreadC(void *drvPvt)
  {
      Lb115Controller *pPvt = (Lb115Controller *)drvPvt;
      pPvt->parseThread();
  }

  // callback for exit hook
  static void exitCallbackC(void *drvPvt)
  {
    Lb115Controller *pPvt = (Lb115Controller *)drvPvt;
    pPvt->stopThreads();
    delete pPvt;
  }

  /** Creates a new Lb115Controller object.
    * Configuration command, called directly or from iocsh
    * \param[in] portName                 The name of the asyn port that will be created for this driver
    * \param[in] asynPortName             The name of the drvAsynXPort that was created previously to connect to the amplifier
    * \param[in] pollPeriod               The time in ms between polls
    */
  int Lb115CreateController(const char *portName, const char *asynPortName, int pollPeriod, 
                            const char* lb115Id = "0001", const char* localId = "9001")
  {
    new Lb115Controller(portName, asynPortName, double(pollPeriod)/1000.0, lb115Id, localId);
    return(asynSuccess);
  }
}

/** Creates a new Lb115Controller object.
  * \param[in] portName                 The name of the asyn port that will be created for this driver
  * \param[in] asynPortName             The name of the drvAsynXPort that was created previously to connect to the amplifier
  * \param[in] waveformMaxPoints        The number of measurement values for the Waveform data (Size of array)
  * \param[in] pollPeriod               The time between polls in milliseconds
  */
Lb115Controller::Lb115Controller(const char *portName, const char *asynPortName, double pollPeriod,
                                 const char* lb115Id, const char* localId)
  :  asynPortDriver(portName,
                         1, 
                         asynInt32Mask | asynInt64Mask | asynFloat64Mask | asynDrvUserMask | asynOctetMask, 
                         asynInt32Mask | asynInt64Mask | asynFloat64Mask | asynOctetMask, 
                         ASYN_CANBLOCK, 
                         1, // autoconnect
                         0, 0),  // Default priority and stack size
      m_pollPeriod(pollPeriod), m_toDevice(lb115Id), m_fromDevice(localId),
      m_repeatCounter(0)  
{

  static const char *functionName = "Lb115Controller::Constructor";

	// create parameters
  // status
  createParam("P_SerialNumber",   asynParamOctet,   &m_indexSerialNumber);
  createParam("P_ProgramVersion", asynParamOctet,   &m_indexProgramVersion);
  createParam("P_KernelVersion",  asynParamOctet,   &m_indexKernelVersion);
  createParam("P_DeviceName",     asynParamOctet,   &m_indexDeviceName);
  createParam("P_MeasEnable",     asynParamInt32,   &m_indexMeasEnabled);
  createParam("P_SetMeasEnable",  asynParamInt32,   &m_indexSetMeasEnable);
  createParam("P_DoseMode",       asynParamInt32,   &m_indexDoseMode);
  createParam("P_Fail",           asynParamInt32,   &m_indexFail);

  for(int i = 0; i < m_digitalInCount; ++i)
  {
    std::string paramName = "P_DI" + std::to_string(i+1);
    createParam(paramName.c_str(), asynParamInt32,   &m_indexDigialIn[i]);
  }

  for(int i = 0; i < m_relaisCount; ++i)
  {
    std::string paramName = "P_Relais" + std::to_string(i+1);
    createParam(paramName.c_str(), asynParamInt32,   &m_indexRelais[i]);
  }

  for(int det = 0; det < m_detectorCount; ++det)
  {
    std::string paramNameBase = "P_Det" + std::to_string(det+1) + "_";
    std::string paramName = paramNameBase + "SwVersion";
    createParam(paramName.c_str(), asynParamOctet,   &m_indexDetector[det].swVersion);
    paramName = paramNameBase + "HwVersion";
    createParam(paramName.c_str(), asynParamOctet,   &m_indexDetector[det].hwVersion);
    paramName = paramNameBase + "Reset";
    createParam(paramName.c_str(), asynParamInt32,   &m_indexDetector[det].reset);
  }

  for(int ch = 0; ch < m_channelCount; ++ch)
  {
    std::string paramNameBase = "P_CH" + std::to_string(ch+1) + "_";
    std::string paramName = paramNameBase + "DetName";
    createParam(paramName.c_str(),   asynParamOctet,   &m_indexChannel[ch].detName);
    paramName = paramNameBase + "IdName";
    createParam(paramName.c_str(),   asynParamOctet,   &m_indexChannel[ch].id);
    paramName = paramNameBase + "RUnit";
    createParam(paramName.c_str(),   asynParamOctet,   &m_indexChannel[ch].rateUnit);
    paramName = paramNameBase + "R";
    createParam(paramName.c_str(),   asynParamFloat64, &m_indexChannel[ch].rateValue);
    paramName = paramNameBase + "State";
    createParam(paramName.c_str(),   asynParamInt32,   &m_indexChannel[ch].state);
    paramName = paramNameBase + "RTime";
    createParam(paramName.c_str(),   asynParamFloat64, &m_indexChannel[ch].rateTime);
    for(int lim = 0; lim < m_alarmPerChannelCount; ++lim)
    {
      paramName = paramNameBase + "L" + std::to_string(lim+1);
      createParam(paramName.c_str(),   asynParamFloat64, &m_indexChannel[ch].rateAlarmValue[lim]);
      paramName = paramNameBase + "statL" + std::to_string(lim+1);
      createParam(paramName.c_str(),   asynParamInt32,   &m_indexChannel[ch].rateAlarmActive[lim]);
    }
    paramName = paramNameBase + "Ts";
    createParam(paramName.c_str(),   asynParamInt64,   &m_indexChannel[ch].timestamp);
    paramName = paramNameBase + "Dt";
    createParam(paramName.c_str(),   asynParamOctet,   &m_indexChannel[ch].datetime);
    paramName = paramNameBase + "H";
    createParam(paramName.c_str(),   asynParamFloat64, &m_indexChannel[ch].doseValue);
    paramName = paramNameBase + "HUnit";
    createParam(paramName.c_str(),   asynParamOctet,   &m_indexChannel[ch].doseUnit);
    paramName = paramNameBase + "HTime";
    createParam(paramName.c_str(),   asynParamFloat64, &m_indexChannel[ch].doseTime);
  }

  // internal
  createParam("P_PollPeriod",     asynParamInt32,   &m_indexPollPeriod);
  createParam("P_CON",            asynParamInt32,   &m_indexConnected);
  createParam("P_COM_FAIL",       asynParamInt32,   &m_indexComFail);

  // Connect to asyn octet port with asynCommonSyncIO 
  asynStatus status = pasynOctetSyncIO->connect(asynPortName, 0, &m_pasynUserController, NULL);
  if (status != asynSuccess)
  {
    asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
        "%s::%s port %s can't connect to asynCommon on Octet server %s.\n",
    driverName, functionName, portName, asynPortName);
    return;
  }

  // the protocol uses carridge return for end of string indicator
  pasynOctetSyncIO->setInputEos(m_pasynUserController, "\r", 1);
  pasynOctetSyncIO->setOutputEos(m_pasynUserController, "\r", 1);

  // Create Events and Threads
  m_requestEventId = epicsEventCreate(epicsEventEmpty);
  epicsThreadId requestThreadId = epicsThreadCreate("Lb115ControllerRequestThread",
                                          epicsThreadPriorityMedium,
                                          epicsThreadGetStackSize(epicsThreadStackMedium),
                                          (EPICSTHREADFUNC)requestThreadC, (void*)this);
  m_parseEventId = epicsEventCreate(epicsEventEmpty);
  epicsThreadId parseThreadId = epicsThreadCreate("Lb115ControllerParseThread",
                                          epicsThreadPriorityMedium,
                                          epicsThreadGetStackSize(epicsThreadStackMedium),
                                          (EPICSTHREADFUNC)parseThreadC, (void*)this);

  // register exit destruction hook
  epicsAtExit(exitCallbackC, this);

  if(   m_requestEventId == 0 || requestThreadId == 0 
     || m_parseEventId == 0 || parseThreadId == 0)
  {
    printf("%s:%s: epicsThreadCreate failure\n", driverName, functionName);
    return;
  }
  
  // Set initial parameters
  setIntegerParam(m_indexPollPeriod, int(m_pollPeriod * 1000));
  setIntegerParam(m_indexConnected, 0);
  init();
}

/** Destructor
 * 
 */
Lb115Controller::~Lb115Controller()
{
  pasynOctetSyncIO->disconnect(m_pasynUserController);
}

void Lb115Controller::report(FILE *fp, int details)
{
  fprintf(fp, "device port: %s\n", this->portName);
  if (details)
  {
      int connected = 0;
      getIntegerParam(m_indexConnected, &connected);
      fprintf(fp, "    connected:          %s\n", connected ? "true" : "false");
      fprintf(fp, "    com fails:          %d times\n", m_comFailCounter);
      fprintf(fp, "    pollPeriod:         %f sec\n", m_pollPeriod);
  }
  asynPortDriver::report(fp, details);
}

void Lb115Controller::stopThreads()
{
  printf("%s - Exiting background threads ...\r\n", driverName);
  m_exitPending = true;
  epicsThreadSleep(1.0);
  epicsEventDestroy(m_requestEventId);
  epicsEventDestroy(m_parseEventId);
}

/** Task to poll relevant parameters every cycle
 *  and send user commands. */
void Lb115Controller::requestThread(void)
{
  const char *functionName = "RequestThread";
  int isConnected = 1;
  lock();
  while(1)
  {
    unlock();
    if (m_pollPeriod > 0.0)
    {
      epicsEventWaitWithTimeout(m_requestEventId, m_pollPeriod);
    }
    else
    {
      epicsEventWait(m_requestEventId);
    }

    if(m_exitPending)
      break;

    lock();

    // Check if connection to device is active and try to reconnect
    getIntegerParam(m_indexConnected, &isConnected);
    if(!isConnected)
    {
      // the asyn autoconnect is handling the port reconnection, here we just try to query if we are connected or not
      int con = 0;
      pasynManager->isConnected(m_pasynUserController, &con);
      if(con)
      {
        setIntegerParam(m_indexConnected, 1);
        printf("%s:%s - Device (re-)connected\r\n", driverName, functionName);
        init();
      }
      continue;
    }

    // process commands in message queue
    if(!m_commandQueue.empty())
    {
      sCommand command = m_commandQueue.front();
      std::string commandString = command.message;

      // prepare request command to Lb115Controller
      asynStatus status = asynSuccess;
      size_t nwrite = 0;
      size_t nread = 0;
      int eomReason;

      // Debug print
      //printf("%s:%s - Request String: %s\r\n", driverName, functionName, commandString.c_str());
      
      // use no timeout if we not expect a response
      double timeout = command.expectsResponse ? m_pollPeriod : 0;

      // send request command to Lb115Controller
      sResponse response;
      response.paramIndex = command.paramIndex;
      response.request = commandString;
      response.channel = command.channel;
      if(command.expectsResponse)    
      {
        status = pasynOctetSyncIO->writeRead(m_pasynUserController,
                        commandString.c_str(), commandString.size(),
                        m_inString, sizeof(m_inString),
                        timeout,
                        &nwrite, &nread, &eomReason);
        response.message = std::string(m_inString);
      }
      else
      {
        status = pasynOctetSyncIO->write(m_pasynUserController,
                        commandString.c_str(), commandString.size(),
                        timeout, &nwrite);
      }

      if(status == asynStatus::asynDisconnected)
      {
        printf("%s:%s - ERROR: Device seems disconnected. Try to reconnect ...\r\n", driverName, functionName);
        // clear the queue and set connected property
        std::queue<sCommand> empty;
        std::swap(m_commandQueue, empty);
        setIntegerParam(m_indexConnected, 0);
        callParamCallbacks();
        continue;
      }
      else if(status > asynStatus::asynSuccess)
      {
        if(m_comFailCounter++ > 50)
        {
          setIntegerParam(m_indexComFail, 1);
          callParamCallbacks();
        }
        asynPrint(m_pasynUserController, ASYN_TRACE_ERROR, "%s:%s - Status %d on command %s (%lu written, %lu read, %d eom)\r\n", 
                  driverName, functionName, status, commandString.c_str(), nwrite, nread, eomReason);
      }

      // Debug print
      //printf("%s:%s - Response String: %s\r\n", driverName, functionName, response.message.c_str());
      
      // add response to message parsing queue
      if(command.expectsResponse)
      {
        if(response.message.length() > 0 && nread > 0)
        {
          if(m_comFailCounter > 10)
          {
            setIntegerParam(m_indexComFail, 0);
            m_comFailCounter = 0;
          }

          m_responseQueue.push(response);
        }
        else
        {
          if(m_repeatCounter >= 3)
          {
            status = asynError;
            asynPrint(m_pasynUserController, ASYN_TRACE_ERROR, "%s:%s - ERROR: Response for command '%s' expected but NOT received after %i requests.\r\n", 
                      driverName, functionName, m_commandQueue.front().message.c_str(), m_repeatCounter);
            m_repeatCounter = 0;
          }
          else
          {
            asynPrint(m_pasynUserController, ASYN_TRACE_ERROR, "%s:%s - ERROR: Response expected for command '%s' but NOT received! Repeating request.\r\n", 
                      driverName, functionName, m_commandQueue.front().message.c_str());
            m_repeatCounter++;
            continue;
          }
        }
      }

      // Debugging output
      if(status)
      {
          epicsSnprintf(m_pasynUserController->errorMessage, m_pasynUserController->errorMessageSize,
                      "%s:%s: status=%i",
                      driverName, functionName, status);
      }
      else
      {
          asynPrint(m_pasynUserController, ASYN_TRACEIO_DRIVER, "%s:%s", driverName, functionName);
      }

      // Delete command from message queue
      m_commandQueue.pop();
    }
    else
    {
      // Push requests to queue which have to be polled periodically
      getDeviceState();
      for(int ch = 0; ch < m_channelCount; ++ch)
      {
        getChannel(ch);
      }
    }
  }
}

/** Task read and parse the incoming messages. */
void Lb115Controller::parseThread(void)
{
  const char *functionName = "ParseThread";
  asynStatus status;

  lock();
  while(1)
  {
    unlock();
    epicsEventWaitWithTimeout(m_parseEventId, m_sleepPeriod);

    if(m_exitPending)
      break;

    lock();

    // Read message queue
    if(!m_responseQueue.empty())
    {
      status = asynSuccess;
      std::string responseString = m_responseQueue.front().message;
      int paramIndex = m_responseQueue.front().paramIndex;

      // checksum
      std::string inputWithoutCsm;
      std::string parsed;
      std::string calculated;
      if(!checkChecksum(responseString, inputWithoutCsm, parsed, calculated))
      {
        //printf("%s:%s - WARN: Checksum for index %d does not match: %s parsed, %s expected\r\n", driverName, functionName, paramIndex, parsed.c_str(), calculated.c_str());
        //return;
      }
      // response with checksum stuff and seperators removed
      responseString = inputWithoutCsm;
      responseString.erase(std::remove(responseString.begin(), responseString.end(), '*'), responseString.end());

      // parse parameters and set values to EPICS Database
      if(paramIndex == m_indexProgramVersion)
      {
        char buffer[20];
        std::string search = m_fromDevice + m_toDevice + m_mapDataPoints.at(eDataPoints::eProgramVersion) + "%[^\t\r\n]";
        std::sscanf(responseString.c_str(), search.c_str(), buffer);
        status = setStringParam(m_indexProgramVersion, buffer);
      }
      else if(paramIndex == m_indexKernelVersion)
      {
        char buffer[20];
        std::string search = m_fromDevice + m_toDevice + m_mapDataPoints.at(eDataPoints::eKernelVersion) + "%[^\t\r\n]*";
        std::sscanf(responseString.c_str(), search.c_str(), buffer);
        status = setStringParam(m_indexKernelVersion, buffer);
      }
      else if(paramIndex == m_indexSerialNumber)
      {
        char buffer[20];
        std::string search = m_fromDevice + m_toDevice + m_mapDataPoints.at(eDataPoints::eSerialNumber) + "%[^\t\r\n]*";
        std::sscanf(responseString.c_str(), search.c_str(), buffer);
        status = setStringParam(m_indexSerialNumber, buffer);
      }
      else if(paramIndex == m_indexDeviceName)
      {
        char buffer[20];
        std::string search = m_fromDevice + m_toDevice + m_mapDataPoints.at(eDataPoints::eDeviceName) + "%[^\t\r\n]*";
        std::sscanf(responseString.c_str(), search.c_str(), buffer);
        status = setStringParam(m_indexDeviceName, buffer);
      }
      else if(paramIndex == m_indexSetMeasEnable)
      {
        // we dont set the feedback here
        // feedback comes with m_indexFail (getDeviceStatus)
      }
      else if(paramIndex == m_indexFail)
      {
        uint32_t state;
        std::string search = m_fromDevice + m_toDevice + m_mapDataPoints.at(eDataPoints::eDeviceState) + "%x";
        if(std::sscanf(responseString.c_str(), search.c_str(), &state))
        {
          for(int i = 0; i < m_digitalInCount; ++i)
          {
            status = setIntegerParam(m_indexDigialIn[i], (state & int(std::pow(2,i+0))) > 0);
          }
          for(int i = 0; i < m_relaisCount; ++i)
          {
            status = setIntegerParam(m_indexRelais[i], (state & int(std::pow(2,i+3))) > 0);
          }
          status = setIntegerParam(m_indexMeasEnabled, ((state & 0x0200) > 0 ? 1 : 0));
          status = setIntegerParam(m_indexDoseMode, (state & 0x0100) > 0);
          status = setIntegerParam(m_indexFail, (state & 0xC000) > 0);
        }
      }
      else
      {
        bool found = false;
        for(int i = 0; i < m_detectorCount; ++i)
        {
          if(paramIndex == m_indexDetector[i].swVersion)
          {
            eDataPoints p = eDataPoints::eInvalidDataPoint;
            switch(i)
            {
              case 0: p = eDataPoints::eDet1SwVersion; break;
              case 1: p = eDataPoints::eDet2SwVersion; break;
              default:
                break;
            }

            if(p != eDataPoints::eInvalidDataPoint)
            {
              char buffer[20];
              std::string search = m_fromDevice + m_toDevice  + m_mapDataPoints.at(p)  + "%[^\t\r\n]";
              std::sscanf(responseString.c_str(), search.c_str(), buffer);
              status = setStringParam(m_indexDetector[i].swVersion, buffer);
              found = true;
              break;
            }
          }
          else if(paramIndex == m_indexDetector[i].hwVersion)
          {
            eDataPoints p = eDataPoints::eInvalidDataPoint;
            switch(i)
            {
              case 0: p = eDataPoints::eDet1HwVersion; break;
              case 1: p = eDataPoints::eDet2HwVersion; break;
              default:
                break;
            }

            if(p != eDataPoints::eInvalidDataPoint)
            {
              char buffer[20];
              std::string search = m_fromDevice + m_toDevice + m_mapDataPoints.at(p)  + "%[^\t\r\n]";
              std::sscanf(responseString.c_str(), search.c_str(), buffer);
              status = setStringParam(m_indexDetector[i].hwVersion, buffer);
              found = true;
              break;
            }
          }
          else if(paramIndex == m_indexDetector[i].reset)
          {
            eDataPoints p = eDataPoints::eInvalidDataPoint;
            switch(i)
            {
              case 0: p = eDataPoints::eResetDoseA; break;
              case 1: p = eDataPoints::eResetDoseB; break;
              default:
                break;
            }

            if(p != eDataPoints::eInvalidDataPoint)
            {
              // no need for feedback
              found = true;
              break;
            }
          }
        }

        if(!found)
        {
          for(int i = 0; i < m_channelCount; ++i)
          {
            if(paramIndex != m_indexChannel[i].state)
              continue;
            
            eDataPoints p = eDataPoints::eInvalidDataPoint;
            switch(i)
            {
              case 0: p = eDataPoints::eDet1Alpha; break;
              case 1: p = eDataPoints::eDet1Beta; break;
              case 2: p = eDataPoints::eDet2Alpha; break;
              case 3: p = eDataPoints::eDet2Beta; break;
              default:
                break;
            }
            if(p != eDataPoints::eInvalidDataPoint)
            {
              char buffer[1001];
              std::string search = m_fromDevice + m_toDevice + m_mapDataPoints.at(p) + "%[^\t\r\n]";
              std::sscanf(responseString.c_str(), search.c_str(), buffer);

              std::vector<std::string> entries;
              tokenize(std::string(buffer), '|', entries);
              for(const std::string &entry : entries)
              {
                size_t index = entry.find(':');
                if(index == std::string::npos)
                  continue;
                
                std::string key = entry.substr(0, index);
                std::string value = entry.substr(index + 1);

                if(key == "detector_name")
                {
                  status = setStringParam(m_indexChannel[i].detName, value);
                }
                else if(key == "meas_id")
                {
                  status = setStringParam(m_indexChannel[i].id, value);
                }
                else if(key == "unit_meas_val")
                {
                  size_t index = value.find(0xb5);
                  if(index != std::string::npos)
                  {
                    value.replace(index, 1, "µ");
                  }
                  status = setStringParam(m_indexChannel[i].rateUnit, value);
                }
                else if(key == "meas_val")
                {
                  float val = 0;
                  if(std::sscanf(value.c_str(), "%f", &val) > 0)
                  {
                    status = setDoubleParam(m_indexChannel[i].rateValue, val);
                  }
                }
                else if(key == "meas_status")
                {
                  uint32_t state = 0;
                  if(std::sscanf(value.c_str(), "%x", &state) > 0)
                  {
                    eChannelStates val = eChannelStates::eInvalidState;
                    if(i == 1 || i == 3)
                    {
                      // alpha channels
                      if(state & 0x2000)
                      {
                        val = eChannelStates::eUnderrange;
                      }
                      else if(state & 0x4000)
                      {
                        val = eChannelStates::eOverrange;
                      }
                      else
                      {
                        val = eChannelStates::eOk;
                      }

                      // alarm
                      for(int lim = 0; lim < m_alarmPerChannelCount; ++lim)
                      {
                        status = setIntegerParam(m_indexChannel[i].rateAlarmActive[lim], state & int(std::pow(2,lim+4)));
                      }
                    }
                    else
                    {
                      // beta channels
                      if(state & 0x8000)
                      {
                        val = eChannelStates::eUnderrange;
                      }
                      else if(state & 0x10000)
                      {
                        val = eChannelStates::eOverrange;
                      }
                      else
                      {
                        val = eChannelStates::eOk;
                      }

                      // alarm
                      for(int lim = 0; lim < m_alarmPerChannelCount; ++lim)
                      {
                        status = setIntegerParam(m_indexChannel[i].rateAlarmActive[lim], state & int(std::pow(2,i+8)));
                      }
                    }

                    if(state & 0x40000)
                    {
                      val = eChannelStates::eTimeout;
                    }
                    else if(state & 0x0001)
                    {
                      val = eChannelStates::eError; // Probefail
                    }
                    if(val != eChannelStates::eInvalidState)
                    {
                      status = setIntegerParam(m_indexChannel[i].state, val);
                    }
                  }
                }
                else if(key == "meas_time")
                {
                  float val = 0;
                  if(std::sscanf(value.c_str(), "%f", &val) > 0)
                  {
                    status = setDoubleParam(m_indexChannel[i].rateTime, val);
                  }
                }
                else if(key == "alarm_limit_1")
                {
                  float val = 0;
                  if(std::sscanf(value.c_str(), "%f", &val))
                  {
                    status = setDoubleParam(m_indexChannel[i].rateAlarmValue[0], val);
                  }
                }
                else if(key == "alarm_limit_2")
                {
                  float val = 0;
                  if(std::sscanf(value.c_str(), "%f", &val))
                  {
                    status = setDoubleParam(m_indexChannel[i].rateAlarmValue[1], val);
                  }
                }
                else if(key == "alarm_limit_3")
                {
                  float val = 0;
                  if(std::sscanf(value.c_str(), "%f", &val))
                  {
                    status = setDoubleParam(m_indexChannel[i].rateAlarmValue[2], val);
                  }
                }
                else if(key == "alarm_limit_4")
                {
                  float val = 0;
                  if(std::sscanf(value.c_str(), "%f", &val))
                  {
                    status = setDoubleParam(m_indexChannel[i].rateAlarmValue[3], val);
                  }
                }
                else if(key == "meas_date")
                {
                  status = setStringParam(m_indexChannel[i].datetime, value);
                  std::tm t{};
                  std::istringstream ss(value);
                  ss >> std::get_time(&t, "%d.%m.%Y %H:%M:%S");
                  if(!ss.fail())
                  {
                    status = setInteger64Param(m_indexChannel[i].timestamp, static_cast<epicsInt64>(mktime(&t)));
                  }
                }
                else if(key == "dose_val")
                {
                  float val = 0;
                  if(std::sscanf(value.c_str(), "%f", &val))
                  {
                    status = setDoubleParam(m_indexChannel[i].doseValue, val);
                  }
                }
                else if(key == "unit_dose_val")
                {
                  size_t index = value.find(0xb5);
                  if(index != std::string::npos)
                  {
                    value.replace(index, 1, "µ");
                  }
                  status = setStringParam(m_indexChannel[i].doseUnit, value);
                }
                else if(key == "dose_time")
                {
                  float val = 0;
                  if(std::sscanf(value.c_str(), "%f", &val))
                  {
                    status = setDoubleParam(m_indexChannel[i].doseTime, val);
                  }
                }
              }
              found = true;
              break;
            }
          }
        }

        if(!found)
        {
          printf("%s:%s - ERROR: Unknown parse index %d: %s\r\n", driverName, functionName, paramIndex, responseString.c_str());
          status = asynError;
        }
      }

      // Important: Store Parameters in EPICS Database
      callParamCallbacks();
      m_responseQueue.pop();
      
      // Debugging output
      if(status)
      {
          epicsSnprintf(m_pasynUserController->errorMessage, m_pasynUserController->errorMessageSize,
                      "%s:%s: status=%i",
                      driverName, functionName, status);
      }
      else
      {
          asynPrint(m_pasynUserController, ASYN_TRACEIO_DRIVER,
                    "%s:%s",
                    driverName, functionName);
      }
    }
  }
  
}

/** Called when asyn clients call pasynInt32->write().
  * The base class implementation simply returns the value from the parameter library.
  * Derived classes rarely need to reimplement this function.
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[out] value Address of the value to read. */
asynStatus Lb115Controller::writeInt32(asynUser *pasynUser, epicsInt32 value)
{ 
  const char *functionName = "writeInt32";
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;
  const char *paramName;

  getParamName(function, &paramName);

  if(function == m_indexPollPeriod)
  {
    m_pollPeriod = double(value)/1000.0;
    printf("Set poll period to %d ms (%f seconds)\r\n", value, m_pollPeriod);
    status = setIntegerParam(function, value);
  }
  else if(function == m_indexSetMeasEnable)
  {
    setEnableMeasurement(value);
  }
  else
  {
    bool found = false;
    for(int i = 0; i < m_detectorCount; ++i)
    {
      if(function == m_indexDetector[i].reset)
      {
        if(value > 0)
        {
          setReset(i);
        }
        found = true;
        break;
      }
    }

    if(!found)
    {
      status = asynPortDriver::writeInt32(pasynUser,value);  
    }
  }

  // debugging output
  if(status)
  {
      epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%i, function=%i, name=%s, value=%i",
                  driverName, functionName, status, function, paramName, value);
  }
  asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,"%s:%s: function=%i, name=%s, value=%i",driverName, functionName, function, paramName, value);   
  return status;
}

/** Set all relevant parameters once during first startup of the IOC for Initialization
 *  of Target Device */
void Lb115Controller::init(void)
{
  getProgramVersion();
  getKernelVersion();
  getSerialNumber();
  getDeviceName();
  for(int det = 0; det < m_detectorCount; ++det)
  {
    getDetVersion(det);
  }

  // parameters without feedback
  setIntegerParam(m_indexComFail, 0);
  setIntegerParam(m_indexSetMeasEnable, 1);
  for(int i = 0; i < m_detectorCount; ++i)
  {
    setIntegerParam(m_indexDetector[i].reset, 0);
  }

  callParamCallbacks();
}

void Lb115Controller::getProgramVersion()
{
  sCommand command;
  command.message = "*" + m_toDevice + m_fromDevice + m_mapDataPoints.at(eDataPoints::eProgramVersion) + "*";
  command.message += calculateChecksum(command.message) + "*";
  command.paramIndex = m_indexProgramVersion;
  command.expectsResponse = true;
  m_commandQueue.push(command);
}

void Lb115Controller::getKernelVersion()
{
  sCommand command;
  command.message = "*" + m_toDevice + m_fromDevice + m_mapDataPoints.at(eDataPoints::eKernelVersion) + "*";
  command.message += calculateChecksum(command.message) + "*";
  command.paramIndex = m_indexKernelVersion;
  command.expectsResponse = true;
  m_commandQueue.push(command);
}

void Lb115Controller::getSerialNumber()
{
  sCommand command;
  command.message = "*" + m_toDevice + m_fromDevice + m_mapDataPoints.at(eDataPoints::eSerialNumber) + "*";
  command.message += calculateChecksum(command.message) + "*";
  command.paramIndex = m_indexSerialNumber;
  command.expectsResponse = true;
  m_commandQueue.push(command);
}

void Lb115Controller::getDeviceName()
{
  sCommand command;
  command.message = "*" + m_toDevice + m_fromDevice + m_mapDataPoints.at(eDataPoints::eDeviceName) + "*";
  command.message += calculateChecksum(command.message) + "*";
  command.paramIndex = m_indexDeviceName;
  command.expectsResponse = true;
  m_commandQueue.push(command); 
}

void Lb115Controller::getDeviceState()
{
  sCommand command;
  command.message = "*" + m_toDevice + m_fromDevice + m_mapDataPoints.at(eDataPoints::eDeviceState) + "*";
  command.message += calculateChecksum(command.message) + "*";
  command.paramIndex = m_indexFail;
  command.expectsResponse = true;
  m_commandQueue.push(command);
}

void Lb115Controller::getDetVersion(epicsInt32 detector)
{
  if(detector < 0 && detector >= m_detectorCount)
    return;

  eDataPoints p = eDataPoints::eInvalidDataPoint;
  switch(detector)
  {
    case 0: p = eDataPoints::eDet1HwVersion; break;
    case 1: p = eDataPoints::eDet2HwVersion; break;
    default:
      return;
  }

  if(p != eDataPoints::eInvalidDataPoint)
  {
    sCommand command;
    command.message = "*" + m_toDevice + m_fromDevice + m_mapDataPoints.at(p) + "*";
    command.message += calculateChecksum(command.message) + "*";
    command.paramIndex = m_indexDetector[detector].hwVersion;
    command.expectsResponse = true;
    command.channel = detector;
    m_commandQueue.push(command);
  }

  p = eDataPoints::eInvalidDataPoint;
  switch(detector)
  {
    case 0: p = eDataPoints::eDet1SwVersion; break;
    case 1: p = eDataPoints::eDet2SwVersion; break;
    default:
      return;
  }
  if(p != eDataPoints::eInvalidDataPoint)
  {
    sCommand command;
    command.message = "*" + m_toDevice + m_fromDevice + m_mapDataPoints.at(p) + "*";
    command.message += calculateChecksum(command.message) + "*";
    command.paramIndex = m_indexDetector[detector].swVersion;
    command.expectsResponse = true;
    command.channel = detector;
    m_commandQueue.push(command);
  }
}

void Lb115Controller::getChannel(epicsInt32 channel)
{
  if(channel < 0 && channel >= m_channelCount)
    return;

  eDataPoints p = eDataPoints::eInvalidDataPoint;
  switch(channel)
  {
    case 0: p = eDataPoints::eDet1Alpha; break;
    case 1: p = eDataPoints::eDet1Beta; break;
    case 2: p = eDataPoints::eDet2Alpha; break;
    case 3: p = eDataPoints::eDet2Beta; break;
    default:
      return;
  }
  if(p != eDataPoints::eInvalidDataPoint)
  {
    sCommand command;
    command.message = "*" + m_toDevice + m_fromDevice + m_mapDataPoints.at(p) + "*";
    command.message += calculateChecksum(command.message) + "*";
    command.paramIndex = m_indexChannel[channel].state;
    command.expectsResponse = true;
    command.channel = channel;
    m_commandQueue.push(command);
  }
}

void Lb115Controller::setEnableMeasurement(epicsInt32 enable)
{
  sCommand command;
  command.message = "*" + m_toDevice + m_fromDevice 
                        + m_mapDataPoints.at(enable > 0 ? eDataPoints::eStartMeas : eDataPoints::eStopMeas) 
                        + "*";
  command.message += calculateChecksum(command.message) + "*";
  command.paramIndex = m_indexSetMeasEnable;
  command.expectsResponse = true;
  m_commandQueue.push(command);
}

void Lb115Controller::setReset(epicsInt32 detector)
{
  if(detector < 0 && detector >= m_detectorCount)
    return;

  sCommand command;
  command.message = "*" + m_toDevice + m_fromDevice 
                        + m_mapDataPoints.at(detector > 0 ? eDataPoints::eResetDoseB : eDataPoints::eResetDoseA) 
                        + "*";
  command.message += calculateChecksum(command.message) + "*";
  command.paramIndex = m_indexDetector[detector].reset;
  command.expectsResponse = true;
  command.channel = detector;
  m_commandQueue.push(command);
}

void Lb115Controller::tokenize(std::string const &str, const char delim,
                               std::vector<std::string> &out)
{
    size_t start;
    size_t end = 0;
 
    while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
}

std::string Lb115Controller::calculateChecksum(const std::string &input)
{
  uint32_t sum = 0;
  for (std::string::size_type i = 0; i < input.size(); i++)
  {
      sum += uint8_t(input[i]);
  }

  std::stringstream stream;
  stream << std::setfill('0') << std::setw(2) << std::uppercase
          << std::hex << sum;
  return stream.str().substr(stream.str().length()-2,2);
}

std::string Lb115Controller::extractChecksum(const std::string &input, std::size_t &startOfCsm)
{
  startOfCsm = std::string::npos; 
  std::size_t beforeCsm = input.find("*", 1);
  if(beforeCsm != std::string::npos)
  {
    std::size_t afterCsm = input.find("*", beforeCsm + 1);
    if(afterCsm != std::string::npos)
    {
      startOfCsm = beforeCsm + 1;
      return input.substr(beforeCsm + 1, afterCsm - beforeCsm - 1);
    }
  }
  return "";
}

bool Lb115Controller::checkChecksum(const std::string &input, std::string &inputWithoutCsm, std::string &parsed, std::string &calculated)
{
  std::size_t startOfCsm;
  inputWithoutCsm = input;
  parsed = extractChecksum(input, startOfCsm);
  if(parsed.length() > 0 && startOfCsm != std::string::npos)
  {
    inputWithoutCsm = input.substr(0, startOfCsm);
    calculated = calculateChecksum(inputWithoutCsm);
    return parsed == calculated;
  }
  // if no checksum was found, consider as OK
  return true;
}


/** Code for iocsh registration */
static const iocshArg Lb115CreateControllerArg0 = {"Port name", iocshArgString};
static const iocshArg Lb115CreateControllerArg1 = {"Asyn port name", iocshArgString};
static const iocshArg Lb115CreateControllerArg2 = {"Poll period (ms)", iocshArgInt};
static const iocshArg Lb115CreateControllerArg3 = {"LB115 ID", iocshArgString};
static const iocshArg Lb115CreateControllerArg4 = {"Local ID", iocshArgString};

static const iocshArg * const Lb115CreateControllerArgs[] = {
                                              &Lb115CreateControllerArg0,
                                              &Lb115CreateControllerArg1,
                                              &Lb115CreateControllerArg2,
                                              &Lb115CreateControllerArg3,
                                              &Lb115CreateControllerArg4};

static const iocshFuncDef Lb115CreateControllerDef = {"Lb115CreateController", 5, Lb115CreateControllerArgs};

static void Lb115ControllerCallFunc(const iocshArgBuf *args)
{
  printf("Lb115Controller:Lb115ControllerCallFunc: Creating LB115 Controller...\n");

  Lb115CreateController(args[0].sval, args[1].sval, args[2].ival, args[3].sval, args[4].sval);
}

static void Lb115Register(void)
{
  iocshRegister(&Lb115CreateControllerDef, Lb115ControllerCallFunc);
}

extern "C" {
epicsExportRegistrar(Lb115Register);
}
