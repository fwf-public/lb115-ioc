# Description
This repository holds the source code for an EPICS IOC to communicate with a [LB115](https://www.berthold.com/en/radiation-protection/products/nuclear-data-processing/data-logger-for-various-detectors-lb-115/) stationary radiation datalogger from Berthold Technologies using EPICS asyn and text-based communication over ethernet.

# Prerequisites
To build and use this IOC, following modules have to be installed on your system:
- `make` (For Windows, you will need to install Strawberry Perl or use a [prebuild](https://epics.anl.gov/download/tools/make-4.2.1-win64.zip)) version. Make sure `make` can be found in `PATH`.
- A c++11 compiler
- [EPICS BASE 7.0+](https://epics-controls.org/resources-and-support/base)
- [ASYN](https://github.com/epics-modules/asyn)
    - (optional) [IPAC](https://github.com/epics-modules/ipac)
    - (optional) [SNCSEQ](https://www-csr.bessy.de/control/SoftDist/sequencer)
        - (optional) [RE2C](http://re2c.org/)
- [CALC](https://github.com/epics-modules/calc)
    - (optional) [EPICS SSCAN](https://github.com/epics-modules/sscan)
- (optional) [ALIVE](https://github.com/epics-modules/alive)

# Build
1. Clone the repository into a self created `{TOP}` directory
2. Make sure the EPICS related environment variables `EPICS_BASE` and `EPICS_HOST_ARCH` are set.
3. Setup the paths for the required support modules in `{TOP}/configure/RELEASE.local` according to your system. The alive module is optional.
4. Build the IOC by using `make` in the `{TOP}` directory. A support library and an application using the support library will be build. 

# Using the support library in your own IOC
To use this module for your own IOC, the mechanism is the same as with any other epics support module. You can look in the `lb115App` subdirectory for an example.
1. In `{TOP}/configure/RELEASE` set the path to the modules `{TOP}`, e.g. `LB115=/etc/epics/support/lb115`
2. In the Makefile of your application, add something like:
```
yourappname_DBD += lb115.dbd
yourappname_LIBS += lb115
```
3. Copy (or create) the needed database files from the libraries `{TOP}/db` directory into your `{TOP}/youappnameApp/Db` directory and adapt the Makefile accordingly. You can also link directly by using `DB += $(LB115)/db/lb115.db`
4. In the IOC startup you need to configure the connection, initialize the driver and load the database:
```
< envPaths
cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/yourappnameApp.dbd"
yourappnameApp_registerRecordDeviceDriver pdbbase

## Asyn Config
drvAsynIpPortConfigure("L0","192.168.43.18:8123",0,0,0)

## Load the lb115 driver
# Lb115CreateController("Name", "Asyn-Port", "Poll period (ms)", "LB115 ID", "Local ID")
Lb115CreateController("TF01", "L0", "1000", "0001", "9001")

## Load record instances
dbLoadRecords("db/lb115.db","P=IC:TF01,PORT=TF01")

## digial I/O
dbLoadRecords("db/lb115di.db","P=IC:TF01,PORT=TF01,ID=1")
dbLoadRecords("db/lb115di.db","P=IC:TF01,PORT=TF01,ID=2")
dbLoadRecords("db/lb115di.db","P=IC:TF01,PORT=TF01,ID=3")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=1")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=2")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=3")
dbLoadRecords("db/lb115relais.db","P=IC:TF01,PORT=TF01,ID=4")

## detector info
dbLoadRecords("db/lb115detector.db","P=IC:TF01,PORT=TF01,DET=1")
dbLoadRecords("db/lb115detector.db","P=IC:TF01,PORT=TF01,DET=2")

## channels
dbLoadRecords("db/lb115channel.db","P=IC:TF01,PORT=TF01,CHANNEL=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=1,LIMIT=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=1,LIMIT=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=1,LIMIT=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=1,LIMIT=4")

dbLoadRecords("db/lb115channel.db","P=IC:TF01,PORT=TF01,CHANNEL=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=2,LIMIT=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=2,LIMIT=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=2,LIMIT=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=2,LIMIT=4")

dbLoadRecords("db/lb115channel.db","P=IC:TF01,PORT=TF01,CHANNEL=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=3,LIMIT=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=3,LIMIT=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=3,LIMIT=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=3,LIMIT=4")

dbLoadRecords("db/lb115channel.db","P=IC:TF01,PORT=TF01,CHANNEL=4")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=4,LIMIT=1")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=4,LIMIT=2")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=4,LIMIT=3")
dbLoadRecords("db/lb115channelalarm.db","P=IC:TF01,PORT=TF01,CHANNEL=4,LIMIT=4")

cd "${TOP}/iocBoot/${IOC}"
iocInit
```

# Running the standalone IOC
In the build process, a simple application will be created, that uses the support module. This IOC opens a tcp connection to the LB115. Run the IOC's executable using the startup script, e.g.:
```
cd {TOP}/iocBoot/lb115App
{TOP}/bin/{EPICS_HOST_ARCH}/lb115App st.cmd
```
Remember that every dependency must be in PATH. If you are on windows, the generated `dllPaths.bat` can be executed before starting the application to modify the path for that command window session.

# UI
A ui file for EpicsQt is available in the sources. You can load it with qegui by using:
```
qegui -e -m="DEVICEID=IC:TF01:,DESCRIPTION=Dose Measurement,NAME=IC-TF.01" ui/lb115_detail_1.ui
```

# Docker image
The contained docker image provides the IOC and all it's dependencies. It can be used as a development environment or for a production deployment.

## Build

### Prerequisite and build dependencies
Building the docker image requires the following tools and libraries:
- [Docker](https://www.docker.com/) (minimum version is 24.x.x)
- [Docker EPICS IOC Base](https://codebase.helmholtz.cloud/fwf/Libraries/dockerfiles/docker-epics-ioc-base) base docker image. Can be retrieved from the registry.

### Build arguments
There are some build options for the docker build. They can be used during the `docker build` command step by using
`--build-arg OPTION=VALUE`.
| Option                | Description              | Default |
|-----------------------|--------------------------|---------|
| `EPICS_CALC_VERSION` | The version of the calc epics support module. This is also the tag to use from the github repository. | R3-7-5 |
| `EPICS_ASYN_VERSION` | The version of the asyn epics support module. This is also the tag to use from the github repository.  | R4-44-2 |
| `BUILD_DATE` | The UTC timestamp of the build in ISO-Format, e.g. `$(date -u +'%Y-%m-%dT%H:%M:%SZ')` |  |
| `BUILD_VERSION` | The sematic version string of the image. |  |
| `VCS_REF` | An identifier from the version control system, e.g. a git SHA |  | 


### Build manually from source
1. Clone this repository and change into the repository directory, where this file is located.
2. Authentificate with the container registry.
```
docker login registry.hzdr.de -u <username>
```
3. Build the docker image by using:
```
docker build --build-arg BUILD_VERSION=1.0 --build-arg VCS_REF=$(git rev-parse --short HEAD) --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') -t lb115-ioc .
```
4. Logout from the registry.
```
docker logout registry.hzdr.de
```

## Deployment

### Runtime arguments
There are some environment options for the container. They can be used during the `docker run` command using `-e OPTION=VALUE` or in a `docker-compose` file .
| Option                | Description              | Default |
|-----------------------|--------------------------|---------|
| `PUID` | The ioc executeable will run in the context of this user id. Set it to a known host user to control file permissions. | 0 (root) |
| `PGID` | The ioc executeable will run in the context of this group id. Set it to a known host group to control file permissions. | 0 (root) |

### Run
1. To get the image, authentificate with the container registry or build the image yourself.
```
docker login registry.hzdr.de -u <username>
```
2. Start the container. Use a bind to get access to the iocBoot and db directory:
```
docker run -v ./ioc-lb115-boot:/usr/local/epics/lb115-ioc/iocBoot/ioclb115 -v ./ioc-lb115-db:/usr/local/epics/lb115-ioc/db -dit --network host --name LB115-IOC registry.hzdr.de/fwf/libraries/epics/lb115-ioc
```
3. Check the logs of the IOC or attach to the ioc shell. To exit the shell without stopping the container use `CTRL-p CTRL-q`.
```
docker logs LB115-IOC
docker attach LB115-IOC
```
4. Logout from the registry.
```
docker logout registry.hzdr.de
```

You can also use `docker-compose` by creating a `docker-compose.yml` file with the following example content:
```yaml
---
version: '3.8'
services:
  lb115:
    image: registry.hzdr.de/fwf/libraries/epics/lb115-ioc:latest
    stdin_open: true
    tty: true
    container_name: LB115-IOC
    environment:
      - PUID=1000
      - PGID=1000
    volumes:
      - ./data/lb115-ioc-boot:/usr/local/epics/lb115-ioc/iocBoot/ioclb115
      - ./data/lb115-ioc-db:/usr/local/epics/lb115-ioc/db
    logging:
      driver: "json-file"
      options:
        max-size: "10m"
        max-file: "5"
    restart: unless-stopped
    network_mode: host
```
You can then use `docker-compose up -d`, `docker-compose stop` and `docker-compose down` to control the container. Logs can be viewed by using `docker logs LB115-IOC` and to access the ioc shell use `docker attach LB115-IOC`.


# Changelog
See git log or [CHANGELOG.md](CHANGELOG.md).

# Contact
[Markus Meyer](@meyer) (FWFI)  
Tel.: 2583  
Email: m.meyer@hzdr.de  